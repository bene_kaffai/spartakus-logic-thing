#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# by: bene
#

import json
from event import Event


class COMPARE(object):

    def __init__(self, name, parent):
        super(COMPARE, self).__init__()
        self.send_mqtt = Event()
        self.CLIENTID = name
        self.TOPIC_OUT = '/spartakus/thing/' + self.CLIENTID + '/output/'
        self.TOPIC_IN = '/spartakus/thing/' + self.CLIENTID + '/input/'
        self.first = '0'
        self.second = '0'
        self.switch = True
        # Json with all I/O infos about this node
        self.IOINFO_JSON = json.dumps({'name': self.CLIENTID, 'status': 1, 'type': 'software', 'input': {'first': 1, 'second': 1, 'report': 1, }, 'output': {'bigger': 1, 'smaller': 1, 'equal': 1, 'report': 1}})

    def handle_input(self, msg):
        #  print("inside: handle_input %s:" % (self.CLIENTID))
        if msg.topic.endswith('trigger'):
            self.compare_func()
        elif msg.topic.endswith('switch'):
            self.switch_func(msg)
        elif msg.topic.endswith('first'):
            self.first = msg.payload.decode("utf-8")
            self.compare_func()
        if msg.topic.endswith('second'):
            self.second = msg.payload.decode("utf-8")
            self.compare_func()

    def switch_func(self, msg):
        if msg.payload.decode("utf-8") == '1':
            self.switch = True
        elif msg.payload.decode("utf-8") == '0':
            self.switch = False

    def compare_func(self):
        try:
            if self.switch is True:
                print("inside: compare_func %s. first: %s%s and second %s%s" % (self.CLIENTID, self.first, type(self.first), self.second, type(self.second)))
                if float(self.first) > float(self.second):
                    self.send_mqtt(self.get_topic('bigger'), 1)
                    self.send_mqtt(self.get_topic('smaller'), 0)
                    self.send_mqtt(self.get_topic('equal'), 0)
                elif float(self.first) < float(self.second):
                    self.send_mqtt(self.get_topic('smaller'), 1)
                    self.send_mqtt(self.get_topic('bigger'), 0)
                    self.send_mqtt(self.get_topic('equal'), 0)
                elif float(self.first) == float(self.second):
                    self.send_mqtt(self.get_topic('equal'), 1)
                    self.send_mqtt(self.get_topic('smaller'), 0)
                    self.send_mqtt(self.get_topic('bigger'), 0)
                else:
                    print("inside: compare_func %s. not compareable. with first: %s%s and second %s%s" % (self.CLIENTID, self.first, type(self.first), self.second, type(self.second)))

        except Exception as e:
            print(e)

    # erzeugt Adressen für eigene input-Topics
    def get_topic(self, target):
        return'/spartakus/thing/' + self.CLIENTID + '/output/' + target
