# README Spartakus Logic Thing #


### SYSTEM ###

Auf Basis eines Protokolls auf MQTT Basis sprechen diverse Entitäten in einem Netzwerk miteinander. Entitäten können hier sowohl Hardwareobjekte sein (WEMOS/ARDUINO Microcontroller mit angeschlossenen Sensoren/Aktoren), fertige IoT Lösungen (Philips HUE, etc.), oder Softwareanwendung (Twitter, Python Skripte, Processing Sketche, Telegram Messenger, etc.). Diese Entitäten sind als Knotenpunkte (Nodes) gedacht. Ihre In- und Outputs können miteinander verknüpft werden.


### Logic Thing ###

Python 3 Script zur Erzeugung von Logikelementen.
In der View tauchen nach start dieses Skripts Menüelemente auf, mit denen neue Nodes erzeugt werden können. Sie erlauben das logische verknüpfen von anderen Entitäten.
* AND vergleicht zwei Zahlen ``` /spartakus/thing/' + self.CLIENTID + '/output/and ``` PAYLOAD ```1 ``` oder ```0 ```
* OR vergleicht zwei Zahlen ``` /spartakus/thing/' + self.CLIENTID + '/output/or ``` PAYLOAD ```1 ``` oder ```0 ```
* COMPARE vergleicht zwei Zahlen ``` /spartakus/thing/' + self.CLIENTID + '/output/bigger```, ```smaller```, oder ```equal``` mit PAYLOAD ```1```
* STRECOMPARE vergleicht zwei STrings ``` /spartakus/thing/' + self.CLIENTID + '/output/equal ``` PAYLOAD ```1 ``` oder ```0 ```


### How do I get set up? ###

* Python 3 installieren
* Python Dependencies installieren: paho.mqtt.client, rethinkdb, json, time ``` pip3 install paho-mqtt rethinkdb```
* einen beliebigen MQTT-Broker im Netzwerk starten ``` Defaults: MQTTP_HOST = '192.168.0.100' MQTTP_PORT = 1883```
* RethinkDB installieren und starten ``` Defaults DB_HOST = 'localhost' DB_PORT = 28015```
* ``` python3 logic.py ``` aus  [Logic](https://bitbucket.org/bene_kaffai/spartakus-logic/overview) starten
* ``` python3 LOGIC_thing.py ``` starten


### Weitere Tools für das Spartakus Framework ###

* [Logic](https://bitbucket.org/bene_kaffai/spartakus-logic/overview) (Kommunikationszentrale des Systems)
* [View](https://bitbucket.org/bene_kaffai/spartakus-view) (grafische Nutzer*Innen-Oberfläche zum nodebasierten erstellen und verwalten von Interkationen zwischen Entitäten)
* [WEMOS](https://bitbucket.org/bene_kaffai/spartakus-wemos-template) und [PYTHON](https://bitbucket.org/bene_kaffai/spartakus-python-template) Templates (um eigene Entitäten im Netzwerk verfügbar zu machen)


### Who do I talk to? ###
* MIT
* Version 0.1
* GUI für das Spartakus Framework als Beispiel für die CeBit 2017
* Repo owner or admin: bene_kaffai, https://bitbucket.org/MaxDemian/, https://bitbucket.org/BenHatscher/
