#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# by: bene
#

import json
from event import Event


class OR(object):

    def __init__(self, name):
        super(OR, self).__init__()
        self.send_mqtt = Event()
        self.CLIENTID = name
        self.TOPIC_OUT = '/spartakus/thing/' + self.CLIENTID + '/output/'
        self.TOPIC_IN = '/spartakus/thing/' + self.CLIENTID + '/input/'
        self.first = '0'
        self.second = '0'
        # Json with all I/O infos about this node
        self.IOINFO_JSON = json.dumps({'name': self.CLIENTID, 'status': 1, 'type': 'software', 'input': {'first': 1, 'second': 1, 'report': 1}, 'output': {'or': 1, 'report': 1}})

    def handle_input(self, msg):
        #  print("inside: handle_input %s:" % (self.CLIENTID))
        if msg.topic.endswith('or'):
            self.func()
        elif msg.topic.endswith('first'):
            self.first = msg.payload.decode("utf-8")
            self.func()
        if msg.topic.endswith('second'):
            self.second = msg.payload.decode("utf-8")
            self.func()

    def func(self):
        #  print("inside: func %s. with first: %s%s and second %s%s" % (self.CLIENTID, self.first, type(self.first), self.second, type(self.second)))
        if not self.first == '0' or not self.second == '0':
            self.send_mqtt(self.get_topic('or'), 1)
        else:
            self.send_mqtt(self.get_topic('or'), 0)

    # erzeugt Adressen für eigene input-Topics
    def get_topic(self, target):
        return'/spartakus/thing/' + self.CLIENTID + '/output/' + target
