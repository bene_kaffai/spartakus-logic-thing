#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# by: bene
#

import paho.mqtt.client as mqtt
import json
import time
from AND_Class import AND
from OR_Class import OR
from COMPARE_Class import COMPARE
from STREQUALS_Class import STREQUALS


##############################################################################
# GLOABAL VARIABLES
##############################################################################

MQTTP_HOST = '192.168.0.100'  # '192.168.0.100'
MQTTP_PORT = 1883
CLIENTID = 'LOGIC_PARENT'
NETWORK_THING = '/spartakus/thing/'
TOPIC_OUT = '/spartakus/thing/' + CLIENTID + '/output/'
TOPIC_IN = '/spartakus/thing/' + CLIENTID + '/input/'
TOPIC_PING = '/spartakus/thing/all/input/ping'

# Json with all I/O infos about this node
IOINFO_JSON = json.dumps({'name': CLIENTID, 'status': 1, 'type': 'menu',
                         'input': {'and': 1, 'or': 1, 'compare': 1, 'strequals': 1, 'remove': 1, 'children': 1, 'report': 1}, 'output': {'children': 1, 'report': 1}})

# stores all the AND / OR child virtual things
andNodes = {}
orNodes = {}
compareNodes = {}
strequalsNodes = {}

##############################################################################
# String Equals CLASS & MQTT CALLBACK
##############################################################################


# mqtt callbacks for AND, add new AND child to list
def init_strequals(msg):
    try:
        message = float(msg.payload.decode("utf-8"))
    except ValueError:
        message = msg.payload.decode("utf-8")
    if message == 0.0:
        print('0 kein neues Objekt')
    else:
        newNode = 'STREQUALS_' + str(len(strequalsNodes))  # create new name (number of AND elements in Dict)
        strequalsNodes[newNode] = STREQUALS(newNode)  # add new AND object to Dictonary
        strequalsNodes[newNode].send_mqtt += send_mqtt  # register callback for thing to send mqtt
        client.subscribe(strequalsNodes[newNode].TOPIC_IN + '#')  # subscribe to all incoming topics for AND child
        client.publish(strequalsNodes[newNode].get_topic('report'), strequalsNodes[newNode].IOINFO_JSON)
        print("inside: init_STREQUALS %s" % (len(strequalsNodes)))


def callback_strequals(msg):
    topic_list = msg.topic.split('/')
    sender_name = msg.topic.split(NETWORK_THING)[1].split('/')[0]
    if sender_name in strequalsNodes:
        strequalsNodes[sender_name].handle_input(msg)
    if 'report' in topic_list:
        client.publish(strequalsNodes[target_name].get_topic('report'), strequalsNodes[target_name].IOINFO_JSON)
    print("inside: callback_STREQUALS")


##############################################################################
# AND CLASS & MQTT CALLBACK
##############################################################################

# mqtt callbacks for AND, add new AND child to list
def init_and(msg):
    try:
        message = float(msg.payload.decode("utf-8"))
    except ValueError:
        message = msg.payload.decode("utf-8")
    if message == 0.0:
        print('0 kein neues Objekt')
    else:
        newNode = 'AND_' + str(len(andNodes))  # create new name (number of AND elements in Dict)
        andNodes[newNode] = AND(newNode)  # add new AND object to Dictonary
        andNodes[newNode].send_mqtt += send_mqtt  # register callback for thing to send mqtt
        client.subscribe(andNodes[newNode].TOPIC_IN + '#')  # subscribe to all incoming topics for AND child
        client.publish(andNodes[newNode].get_topic('report'), andNodes[newNode].IOINFO_JSON)
        print("inside: init_and %s" % (len(andNodes)))


def callback_and(msg):
    topic_list = msg.topic.split('/')
    sender_name = msg.topic.split(NETWORK_THING)[1].split('/')[0]
    if sender_name in andNodes:
        andNodes[sender_name].handle_input(msg)
    if 'report' in topic_list:
        client.publish(andNodes[target_name].get_topic('report'), andNodes[target_name].IOINFO_JSON)
    print("inside: callback_and")


##############################################################################
# OR CLASS & MQTT CALLBACK
##############################################################################

# mqtt callbacks for OR, add new OR child to list
def init_or(msg):
    try:
        message = float(msg.payload.decode("utf-8"))
    except ValueError:
        message = msg.payload.decode("utf-8")
    if message == 0.0:
        print('0 kein neues Objekt')
    else:
        newNode = 'OR_' + str(len(orNodes))  # create new name (number of AND elements in Dict)
        orNodes[newNode] = OR(newNode)  # add new AND object to Dictonary
        orNodes[newNode].send_mqtt += send_mqtt  # register callback for thing to send mqtt
        client.subscribe(orNodes[newNode].TOPIC_IN + '#')  # subscribe to all incoming topics for AND child
        client.publish(orNodes[newNode].get_topic('report'), orNodes[newNode].IOINFO_JSON)
        print("inside: init_or %s" % (len(orNodes)))


def callback_or(msg):
    topic_list = msg.topic.split('/')
    sender_name = msg.topic.split(NETWORK_THING)[1].split('/')[0]
    if sender_name in orNodes:
        orNodes[sender_name].handle_input(msg)
    if 'report' in topic_list:
        client.publish(orNodes[target_name].get_topic('report'), orNodes[target_name].IOINFO_JSON)
    print("inside: callback_or")


##############################################################################
# COMPARE CLASS & MQTT CALLBACK
##############################################################################

def init_compare(msg):
    try:
        message = float(msg.payload.decode("utf-8"))
    except ValueError:
        message = msg.payload.decode("utf-8")
    if message == 0.0:
        print('0 kein neues Objekt')
    else:
        newNode = 'COMPARE_' + str(len(compareNodes))  # create new name (number of AND elements in Dict)
        compareNodes[newNode] = COMPARE(newNode, CLIENTID)  # add new AND object to Dictonary
        compareNodes[newNode].send_mqtt += send_mqtt  # register callback for thing to send mqtt
        client.subscribe(compareNodes[newNode].TOPIC_IN + '#')  # subscribe to all incoming topics for AND child
        client.publish(compareNodes[newNode].get_topic('report'), compareNodes[newNode].IOINFO_JSON)
        print("inside: init_compare %s" % (len(compareNodes)))


def callback_compare(msg):
    sender_name = msg.topic.split(NETWORK_THING)[1].split('/')[0]
    if sender_name in compareNodes:
        compareNodes[sender_name].handle_input(msg)
    print("inside: callback_compare")


##############################################################################
# MQTT CALLBACK
##############################################################################

def remove_child(msg):
    global andNodes
    global orNodes
    global compareNodes
    for key in andNodes:
        client.publish('/spartakus/thing/LOGIC/input/thing/remove', andNodes[key].CLIENTID)
    for key in orNodes:
        client.publish('/spartakus/thing/LOGIC/input/thing/remove', orNodes[key].CLIENTID)
    for key in compareNodes:
        client.publish('/spartakus/thing/LOGIC/input/thing/remove', compareNodes[key].CLIENTID)

    andNodes = {}
    orNodes = {}
    compareNodes = {}
    print('removed all children')


# default callback if no specific topic is registered
def default_msg(msg):
    print("default_msg: " + msg.topic + " " + str(msg.payload) + "\n")


# send infos about in- and outnodes
def io_report(msg):
    print("inside io_report")
    send_mqtt(TOPIC_OUT + 'report', IOINFO_JSON)  # send report info on first connection to broker
    # TODO publish children

# register all callbacks for different topics of Parent and Child things
mqtt_callback = {TOPIC_IN + 'report': io_report,
                 TOPIC_PING: io_report,
                 TOPIC_IN + 'remove': remove_child,
                 TOPIC_IN + 'and': init_and,
                 TOPIC_IN + 'or': init_or,
                 TOPIC_IN + 'compare': init_compare,
                 TOPIC_IN + 'strequals': init_strequals,
                 '/spartakus/thing/STREQUALS_': callback_strequals,
                 '/spartakus/thing/AND_': callback_and,
                 '/spartakus/thing/OR_': callback_or,
                 '/spartakus/thing/COMPARE_': callback_compare}


# returns matching Dictonary Key to topic
def get_matching(topic):
    for key in mqtt_callback.keys():
        if topic.startswith(key):
            return key
        else:
            None


def send_mqtt(topic, payload):
    client.publish(topic, payload)


def on_connect(client, userdata, flags, rc):
    # print("Connected with result code " + str(rc))
    client.subscribe(TOPIC_IN + '#')  # subscribe to all incoming topics
    client.subscribe(TOPIC_PING)  # subscribe to global ping
    io_report(1)


def on_message(client, userdata, msg):
    # print(msg.topic + " " + str(msg.payload))
    callback = mqtt_callback.get(get_matching(msg.topic), default_msg)
    callback(msg)

client = mqtt.Client(CLIENTID)
client.on_connect = on_connect  # register callback for event
client.on_message = on_message  # register callback for event

client.connect(MQTTP_HOST, MQTTP_PORT, 60)


##############################################################################
# MAIN LOOP
##############################################################################

try:
    while True:
        client.loop()
        # time.sleep(0.1)
except KeyboardInterrupt:
    remove_child(1)
    client.publish('/spartakus/thing/LOGIC/input/thing/remove', CLIENTID)
    print('interrupted!')
print('bye bye!')
